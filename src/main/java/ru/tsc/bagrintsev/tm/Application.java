package ru.tsc.bagrintsev.tm;

import static ru.tsc.bagrintsev.tm.constant.CommandLineConst.*;

/**
 * @author Sergey Bagrintsev
 * @version 1.5.0
 */

public final class Application {

    public static void main(final String[] args) {
        process(args);
    }

    private static void process(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case HELP:
            case HELP_SHORT:
                showHelp();
                break;
            case VERSION:
            case VERSION_SHORT:
                showVersion();
                break;
            case ABOUT:
                showAbout();
                break;
            default:
                showError(arg); 
        }
    }

    private static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%2s or %-19s Print version.\n", VERSION_SHORT, VERSION);
        System.out.printf("%2s or %-19s Print help.\n", HELP_SHORT, HELP);
        System.out.printf("%-25s Print about.\n", ABOUT);
        System.exit(0);
    }

    private static void showVersion() {
        System.out.println("task-manager version 1.5.0");
        System.exit(0);
    }

    private static void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("Name: Sergey Bagrintsev");
        System.out.println("E-mail: sbagrintsev@t1-consulting.com");
        System.exit(0);
    }

    private static void showError(final String arg) {
        System.err.printf("Error! Argument '%s' is not supported...%n", arg);
    }

}
